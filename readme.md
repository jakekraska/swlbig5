# Recreating the relationship between Subjective Wellbeing and Personality using Machine Learning: An Investigation into Facebook Online Opinions
Ally Marinucci (Monash University), Jake Kraska (Monash University) and Shane Costello (Monash University)

Original data files can be accessed at mypersonality.org 

Requires: demog.csv, big5.csv, swl.csv, user-likes.csv, fb_like.csv

All data files should be placed in ~/data/

This research was supported in part by the Monash eResearch Centre and eSolutions-Research Support Services using the MonARCH HPC Cluster.