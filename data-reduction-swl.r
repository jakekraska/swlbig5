# Part 1: Recreating the relationship between Subjective Wellbeing and Personality using Machine Learning: An Investigation into Facebook Online Opinions
# loads demographics, big5, swl and ul from mypersonality.org and creates a reduced users file and user-likes file for later processing
# by Ally Marinucci (Monash University), Jake Kraska (Monash University) and Shane Costello (Monash University)

# set the working directory to source file directory manually
# ensure all necessary data is in ~/data/
# data required: demog.csv, big5.csv, swl.csv, user-likes.csv
# produces: users-initial-swl.csv, users-reduced-swl.csv, user-likes-swl.csv

# start a clock
cat("Timer started!\n")
ptm <- proc.time()

# Load required packages
library(dplyr)

# read in files
demog <- read.csv("data/demog.csv", header = TRUE)
big5 <- read.csv("data/big5.csv", header = TRUE)
swl <- read.csv("data/swl.csv", header = TRUE)
ul <- read.csv("data/user-likes.csv", header = TRUE) # use the large file "user-likes" not "users-likes"

# Stop the clock
cat("Time taken to read in data\n")
proc.time() - ptm

# Start the clock
ptm <- proc.time()

# Merging the files
step1 <- merge(demog, big5, by = "userid")
step2 <- merge(step1, swl, by = "userid")

# Select required variables and include in new dataframe
users <- subset(step2, select = c("userid", "ope", "con", "ext", "agr", "neu", "swl"))

# create initial users file
write.csv(users, file = "data/users-initial-swl.csv", row.names = FALSE)

# remove missing data from users and ul
users <- users[users$userid %in% ul$userid, ]
ul <- ul[ul$userid %in% users$userid, ]

# save final list of users and the ul to csv
write.csv(users, file = "data/users-reduced-swl.csv", row.names = FALSE)
write.csv(ul, file = "data/user-likes-swl.csv", row.names = FALSE)

# Stop the clock
cat("Time taken to clean data\n")
proc.time() - ptm

# end session
rm(list = ls())
quit(save = "no", status = 0, runLast = TRUE)