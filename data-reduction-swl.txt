Start of Program

R version 3.4.1 (2017-06-30) -- "Single Candle"
Copyright (C) 2017 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> # start a clock
> cat("Timer started!\n")
Timer started!
> ptm <- proc.time()
> 
> # Load required packages
> library(dplyr)

Attaching package: ‘dplyr’

The following objects are masked from ‘package:stats’:

    filter, lag

The following objects are masked from ‘package:base’:

    intersect, setdiff, setequal, union

> 
> # read in files
> demog <- read.csv("demog.csv", header = TRUE)
> big5 <- read.csv("big5.csv", header = TRUE)
> swl <- read.csv("swl.csv", header = TRUE)
> ul <- read.csv("user-likes.csv", header = TRUE) # use the large file "user-likes" not "users-likes"
> likes <- read.csv("fb_like.csv", header = TRUE)
> 
> # Stop the clock
> cat("Time taken to read in data\n")
Time taken to read in data
> proc.time() - ptm
     user    system   elapsed 
10320.764   426.516 10759.123 
> 
> # Start the clock
> ptm <- proc.time()
> 
> # Merging the files
> step1 <- merge(demog, big5, by = "userid")
> step2 <- merge(step1, swl, by = "userid")
> 
> # Select required variables and include in new dataframe
> users <- subset(step2, select = c("userid", "ope", "con", "ext", "agr", "neu", "swl"))
> 
> # create initial users file
> write.csv(users, file = "users-initial-swl.csv", row.names = FALSE)
> 
> # remove missing data from users and ul
> users <- users[users$userid %in% ul$userid, ]
> ul <- ul[ul$userid %in% users$userid, ]
> 
> # save final list of users and the ul to csv
> write.csv(users, file = "users-final-swl.csv", row.names = FALSE)
> write.csv(ul, file = "user-likes-swl.csv", row.names = FALSE)
> 
> # Stop the clock
> cat("Time taken to clean data\n")
Time taken to clean data
> proc.time() - ptm
   user  system elapsed 
290.034  23.550 313.820 
> 
> # end session
> rm(list = ls())
> quit(save = "no", status = 0, runLast = TRUE)
End of program
